window.addEventListener('DOMContentLoaded', (event) => {
    const appendMsg = (msg) => {
        let theDiv = document.querySelector(".out");
        let content = document.createTextNode(msg);
        theDiv.appendChild(content);
    }
    const url = 'ws://localhost:8098'
const connection = new WebSocket(url)

connection.onopen = () => {
  connection.send('hey') 
}

connection.onerror = (error) => {
    appendMsg(`WebSocket error: ${error}`)
}

connection.onmessage = (e) => {
    appendMsg(e.data)
}
});

